import { Containers } from "./module.containers";
import { Spawner } from "./module.spawner";

export module SpawnFeeder {
    export function assign(creep: Creep) {
        let localCreep = creep;
        creep.memory.runJob = () => {
            runJob(localCreep);
        }
    }

    function runJob(creep: Creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
        }

        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
        }

        Containers.assign(creep, !creep.memory.assigned);
        Spawner.assign(creep);

        if (creep.transfer(Game.spawns[creep.memory.spawn], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(Game.spawns[creep.memory.spawn]);
        }

        if (!creep.memory.restock) {
            if (creep.transfer(Game.spawns[creep.memory.spawn], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(Game.spawns[creep.memory.spawn]);
            }
        } else {
            if (creep.withdraw(creep.memory.container, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container);
            }
        }
    }
}