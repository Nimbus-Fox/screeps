"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_containers_1 = require("./module.containers");
var module_spawner_1 = require("./module.spawner");
var SpawnFeeder;
(function (SpawnFeeder) {
    function assign(creep) {
        var localCreep = creep;
        creep.memory.runJob = function () {
            runJob(localCreep);
        };
    }
    SpawnFeeder.assign = assign;
    function runJob(creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
        }
        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
        }
        module_containers_1.Containers.assign(creep, !creep.memory.assigned);
        module_spawner_1.Spawner.assign(creep);
        if (creep.transfer(Game.spawns[creep.memory.spawn], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(Game.spawns[creep.memory.spawn]);
        }
        if (!creep.memory.restock) {
            if (creep.transfer(Game.spawns[creep.memory.spawn], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(Game.spawns[creep.memory.spawn]);
            }
        }
        else {
            if (creep.withdraw(creep.memory.container, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container);
            }
        }
    }
})(SpawnFeeder = exports.SpawnFeeder || (exports.SpawnFeeder = {}));
