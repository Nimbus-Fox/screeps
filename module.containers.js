"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_counters_1 = require("./module.counters");
var module_storage_1 = require("./module.storage");
var Containers;
(function (Containers) {
    function assign(creep, newContainer) {
        var assigned = module_storage_1.Storage.GetValue("containerAssignment");
        var structures = creep.room.find(FIND_STRUCTURES);
        var containers = [];
        for (var name in structures) {
            if (structures.hasOwnProperty(name)) {
                var structure = structures[name];
                if (structure.structureType === STRUCTURE_CONTAINER) {
                    containers.push(structure);
                }
            }
        }
        if (newContainer || !assigned || !assigned[creep.id]) {
            assigned[creep.id] = containers[module_counters_1.Counters.getRandomInt(0, containers.length)].id;
            module_storage_1.Storage.SetValue("containerAssignment", assigned);
        }
        creep.memory.container = Game.getObjectById(assigned[creep.id]);
    }
    Containers.assign = assign;
    if (!module_storage_1.Storage.Exists("containerAssignement")) {
        module_storage_1.Storage.SetValue("containerAssignment", {});
    }
})(Containers = exports.Containers || (exports.Containers = {}));
