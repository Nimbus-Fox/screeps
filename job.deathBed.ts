import {Containers} from "./module.containers";

export module DeathBed {
    export function assign(creep: Creep) {
        let localCreep = creep;
        creep.memory.runJob = () => {
            runJob(localCreep);
        }
    }

    function runJob(creep: Creep) {
        Containers.assign(creep, false);

        if (creep.transfer(creep.memory.container, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(creep.memory.container);
        }
    }
}