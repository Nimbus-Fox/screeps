"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_sources_1 = require("./module.sources");
var module_containers_1 = require("./module.containers");
var Harvester;
(function (Harvester) {
    function assign(creep) {
        var localCreep = creep;
        creep.memory.runJob = function () {
            runJob(localCreep);
        };
    }
    Harvester.assign = assign;
    function runJob(creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
            creep.memory.assignedContainer = true;
        }
        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
            creep.memory.assignedContainer = false;
        }
        module_sources_1.Sources.assign(creep, !creep.memory.assigned);
        module_containers_1.Containers.assign(creep, !creep.memory.assignedContainer);
        if (creep.memory.restock) {
            if (creep.harvest(creep.memory.source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.source, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
        else {
            if (creep.transfer(creep.memory.container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
    }
})(Harvester = exports.Harvester || (exports.Harvester = {}));
