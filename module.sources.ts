import { Counters } from "./module.counters";
import { Storage } from "./module.storage";

export module Sources {
    export function assign(creep: Creep, newSource: boolean) {
        var assigned = Storage.GetValue("assigned");

        var sources = creep.room.find(FIND_SOURCES) as Array<Source>;

        if (newSource || !assigned || !assigned[creep.id]) {
            assigned[creep.id] = sources[Counters.getRandomInt(0, sources.length)].id;
            Storage.SetValue("assigned", assigned);
        }

        creep.memory.source = Game.getObjectById(assigned[creep.id]) as Source;
    }

    if (!Storage.Exists("assigned")) {
        Storage.SetValue("assigned", {});
    }
}