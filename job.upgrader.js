"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_containers_1 = require("./module.containers");
var Upgrader;
(function (Upgrader) {
    function assign(creep) {
        var localCreep = creep;
        creep.memory.runJob = function () {
            runJob(localCreep);
        };
    }
    Upgrader.assign = assign;
    function runJob(creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
        }
        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
        }
        module_containers_1.Containers.assign(creep, !creep.memory.assigned);
        if (creep.memory.restock) {
            if (creep.withdraw(creep.memory.container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
        else {
            if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
    }
})(Upgrader = exports.Upgrader || (exports.Upgrader = {}));
