"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_counters_1 = require("./module.counters");
var module_creeps_1 = require("./module.creeps");
var module_storage_1 = require("./module.storage");
var module_spawn_1 = require("./module.spawn");
var creepsPerClass = {
    harvester: 5,
    spawnFeeders: 2,
    upgraders: 2,
    builders: 5,
    repairers: 3,
    upgrade2: true
};
var Spawner;
(function (Spawner) {
    function assign(creep) {
        var spawn = Game.spawns[module_spawn_1.Spawn.getSpawner(creep)];
        if (spawn.energy < spawn.energyCapacity) {
            creep.memory.spawner = spawn;
            return;
        }
        var extensions = [];
        for (var key in Game.structures) {
            if (Game.structures.hasOwnProperty(key)) {
                var structure = Game.structures[key];
                if (structure.structureType === STRUCTURE_EXTENSION) {
                    if (!creep.memory.spawner) {
                        creep.memory.spawner = structure;
                        continue;
                    }
                    if (structure.energy < creep.memory.spawner.energy) {
                        creep.memory.spawner = structure;
                    }
                }
            }
        }
    }
    Spawner.assign = assign;
    function spawn() {
        var checkCounter = module_storage_1.Storage.GetValue("checkCounter");
        for (var spawnerName in Game.spawns) {
            if (Game.spawns.hasOwnProperty(spawnerName)) {
                var spawner = Game.spawns[spawnerName];
                if (!module_storage_1.Storage.Exists("creepCount" + spawnerName)) {
                    module_storage_1.Storage.SetValue("creepCount" + spawnerName, module_counters_1.Counters.getCreepNumbers(spawnerName));
                }
                var creeps = module_storage_1.Storage.GetValue("creepCount" + spawnerName);
                if (spawner.energy >= 300) {
                    if (creeps.spawnFeeders < creepsPerClass.spawnFeeders && creeps.harvesters !== 0) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "spawnFeeder", [CARRY, CARRY, CARRY, MOVE], 2);
                        continue;
                    }
                    if (creeps.harvesters < creepsPerClass.harvester) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "harvester", [WORK, CARRY, CARRY, MOVE], 2);
                        continue;
                    }
                    if (creeps.upgraders < creepsPerClass.upgraders) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "upgrader", [WORK, CARRY, MOVE, MOVE], 2);
                        continue;
                    }
                    if (creeps.builders < creepsPerClass.builders) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "builder", [WORK, WORK, CARRY, MOVE], 2);
                        continue;
                    }
                    if (creeps.repairers < creepsPerClass.repairers) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "repairer", [WORK, CARRY, MOVE, MOVE], 2);
                        continue;
                    }
                }
                if (spawner.energy >= 200 && !creepsPerClass.upgrade2) {
                    if (creeps.spawnFeeders < creepsPerClass.spawnFeeders && creeps.harvesters !== 0) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "spawnFeeder", [WORK, CARRY, MOVE]);
                        continue;
                    }
                    if (creeps.harvesters < creepsPerClass.harvester) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "harvester", [WORK, CARRY, MOVE]);
                        continue;
                    }
                    if (creeps.upgraders < creepsPerClass.upgraders) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "upgrader", [WORK, CARRY, MOVE]);
                        continue;
                    }
                    if (creeps.builders < creepsPerClass.builders) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "builder", [WORK, CARRY, MOVE]);
                        continue;
                    }
                    if (creeps.repairers < creepsPerClass.repairers) {
                        module_creeps_1.Creeps.createCreeps(spawnerName, "repairer", [WORK, CARRY, MOVE]);
                        continue;
                    }
                }
                if (checkCounter === 250 || checkCounter === 500 || checkCounter === 750 || checkCounter === 950) {
                    module_storage_1.Storage.SetValue("creepCount" + spawnerName, module_counters_1.Counters.getCreepNumbers(spawnerName));
                }
            }
        }
    }
    Spawner.spawn = spawn;
})(Spawner = exports.Spawner || (exports.Spawner = {}));
