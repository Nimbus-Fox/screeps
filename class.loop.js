"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var job_harvester_1 = require("./job.harvester");
var job_upgrader_1 = require("./job.upgrader");
var job_builder_1 = require("./job.builder");
var job_repairer_1 = require("./job.repairer");
var job_spawnFeeder_1 = require("./job.spawnFeeder");
var job_deathBed_1 = require("./job.deathBed");
var module_spawner_1 = require("./module.spawner");
var module_spawn_1 = require("./module.spawn");
var module_storage_1 = require("./module.storage");
/*setTimeout(() => {
        for (let name in Game.creeps) {
            if (Game.creeps.hasOwnProperty(name)) {
                let creep = Game.creeps[name];

                Creeps.assignSource(creep, creep.room.find(FIND_SOURCES));
            }
        }
}, 600000);*/
var Loop;
(function (Loop) {
    function loop() {
        for (var name_1 in Game.creeps) {
            if (Game.creeps.hasOwnProperty(name_1)) {
                var creep = Game.creeps[name_1];
                if (creep.ticksToLive < 50) {
                    creep.memory.class = "deathBed";
                }
                creep.memory.spawn = module_spawn_1.Spawn.getSpawner(creep);
                switch (creep.memory.class) {
                    case "harvester":
                        job_harvester_1.Harvester.assign(creep);
                        break;
                    case "upgrader":
                        job_upgrader_1.Upgrader.assign(creep);
                        break;
                    case "builder":
                        job_builder_1.Builder.assign(creep);
                        break;
                    case "repairer":
                        job_repairer_1.Repairer.assign(creep);
                        break;
                    case "spawnFeeder":
                        job_spawnFeeder_1.SpawnFeeder.assign(creep);
                        break;
                    case "deathBed":
                        job_deathBed_1.DeathBed.assign(creep);
                        break;
                    default:
                        break;
                }
                if (creep.memory.runJob) {
                    creep.memory.runJob();
                }
            }
        }
        var checkCounter = module_storage_1.Storage.GetValue("checkCounter");
        module_storage_1.Storage.SetValue("checkCounter", checkCounter + 1);
        if (checkCounter === 1000) {
            module_storage_1.Storage.SetValue("checkCounter", 0);
            StorageCheck();
        }
        module_spawner_1.Spawner.spawn();
    }
    Loop.loop = loop;
    if (!module_storage_1.Storage.Exists("checkCounter")) {
        module_storage_1.Storage.SetValue("checkCounter", 0);
    }
    function StorageCheck() {
        sourceAssignCheck();
        containerAssignCheck();
        spawnerAssignCheck();
    }
    function sourceAssignCheck() {
        var assigned = module_storage_1.Storage.GetValue("assigned");
        for (var key in assigned) {
            if (assigned.hasOwnProperty(key)) {
                var deleteCreep = true;
                for (var name in Game.creeps) {
                    if (Game.creeps.hasOwnProperty(name)) {
                        if (Game.creeps[name].id === key) {
                            deleteCreep = false;
                        }
                    }
                }
                if (deleteCreep) {
                    delete assigned[key];
                }
            }
        }
        module_storage_1.Storage.SetValue("assigned", assigned);
    }
    function containerAssignCheck() {
        var assigned = module_storage_1.Storage.GetValue("containerAssignment");
        for (var key in assigned) {
            if (assigned.hasOwnProperty(key)) {
                var deleteCreep = true;
                for (var name in Game.creeps) {
                    if (Game.creeps.hasOwnProperty(name)) {
                        if (Game.creeps[name].id === key) {
                            deleteCreep = false;
                        }
                    }
                }
                if (deleteCreep) {
                    delete assigned[key];
                }
            }
        }
        module_storage_1.Storage.SetValue("containerAssignment", assigned);
    }
    function spawnerAssignCheck() {
        var assigned = module_storage_1.Storage.GetValue("creepSpawners");
        for (var key in assigned) {
            if (assigned.hasOwnProperty(key)) {
                var deleteCreep = true;
                for (var name in Game.creeps) {
                    if (Game.creeps.hasOwnProperty(name)) {
                        if (Game.creeps[name].name === key) {
                            deleteCreep = false;
                        }
                    }
                }
                if (deleteCreep) {
                    delete assigned[key];
                }
            }
        }
    }
})(Loop = exports.Loop || (exports.Loop = {}));
