"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_counters_1 = require("./module.counters");
var module_storage_1 = require("./module.storage");
var module_spawn_1 = require("./module.spawn");
var Creeps;
(function (Creeps) {
    function createCreeps(spawn, classString, body, gen) {
        if (gen === void 0) { gen = 1; }
        var random = module_counters_1.Counters.getRandomInt(1, 100);
        while (Game.spawns[spawn].createCreep(body, classString + "lv:" + gen + "-" + random.toString(), { "class": classString }) == -3) {
            random = module_counters_1.Counters.getRandomInt(1, 100);
        }
        module_spawn_1.Spawn.setSpawner(classString + "lv:" + gen + "-" + random.toString(), spawn);
        module_storage_1.Storage.SetValue("creepCount" + spawn, module_counters_1.Counters.getCreepNumbers(spawn));
    }
    Creeps.createCreeps = createCreeps;
})(Creeps = exports.Creeps || (exports.Creeps = {}));
