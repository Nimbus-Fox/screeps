interface CreepMemory {
    class?: string;
    building?: boolean;
    spawn?: string;
    spawner?: Spawn | Extension;
    runJob?: Function;
    restock?: boolean;
    assigned?: boolean;
    source?: Source;
    container?: Container;
    assignedContainer?: boolean;
}

interface IHarvester extends CreepMemory {

}

interface IJob {
    run: Function;
}

interface Memory {
    storage: Storage;
}

interface Storage {
    assigned: { [key: string]: Source };
    count: Object;
}

interface ICreepCount {
    builders: number;
    upgraders: number;
    harvesters: number;
    repairers: number;
    spawnFeeders: number;
}