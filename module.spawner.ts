import { Counters } from "./module.counters";
import { Creeps } from "./module.creeps";
import { Storage } from "./module.storage";
import { Spawn } from "./module.spawn";

var creepsPerClass = {
    harvester: 5,
    spawnFeeders: 2,
    upgraders: 2,
    builders: 5,
    repairers: 3,
    upgrade2: true
};

export module Spawner {

    export function assign(creep: Creep) {
        var spawn = Game.spawns[Spawn.getSpawner(creep)];

        if (spawn.energy < spawn.energyCapacity) {
            creep.memory.spawner = spawn;
            return;
        }

        var extensions: Array<Extension> = [];

        for (var key in Game.structures) {
            if (Game.structures.hasOwnProperty(key)) {
                var structure = Game.structures[key];

                if (structure.structureType === STRUCTURE_EXTENSION) {
                    if (!creep.memory.spawner) {
                        creep.memory.spawner = structure as Extension;
                        continue;
                    }

                    if ((structure as Extension).energy < creep.memory.spawner.energy) {
                        creep.memory.spawner = structure as Extension;
                    }
                }
            }
        }
    }

    export function spawn() {
        var checkCounter = Storage.GetValue<number>("checkCounter");
        for (let spawnerName in Game.spawns) {
            if (Game.spawns.hasOwnProperty(spawnerName)) {
                var spawner = Game.spawns[spawnerName];

                if (!Storage.Exists("creepCount" + spawnerName)) {
                    Storage.SetValue("creepCount" + spawnerName, Counters.getCreepNumbers(spawnerName));
                }

                var creeps = Storage.GetValue<ICreepCount>("creepCount" + spawnerName);

                if (spawner.energy >= 300) {
                    if (creeps.spawnFeeders < creepsPerClass.spawnFeeders && creeps.harvesters !== 0) {
                        Creeps.createCreeps(spawnerName, "spawnFeeder", [CARRY, CARRY, CARRY, MOVE], 2);
                        continue;
                    }

                    if (creeps.harvesters < creepsPerClass.harvester) {
                        Creeps.createCreeps(spawnerName, "harvester", [WORK, CARRY, CARRY, MOVE], 2);
                        continue;
                    }

                    if (creeps.upgraders < creepsPerClass.upgraders) {
                        Creeps.createCreeps(spawnerName, "upgrader", [WORK, CARRY, MOVE, MOVE], 2);
                        continue;
                    }

                    if (creeps.builders < creepsPerClass.builders) {
                        Creeps.createCreeps(spawnerName, "builder", [WORK, WORK, CARRY, MOVE], 2);
                        continue;
                    }

                    if (creeps.repairers < creepsPerClass.repairers) {
                        Creeps.createCreeps(spawnerName, "repairer", [WORK, CARRY, MOVE, MOVE], 2);
                        continue;
                    }
                }

                if (spawner.energy >= 200 && !creepsPerClass.upgrade2) {
                    if (creeps.spawnFeeders < creepsPerClass.spawnFeeders && creeps.harvesters !== 0) {
                        Creeps.createCreeps(spawnerName, "spawnFeeder", [WORK, CARRY, MOVE]);
                        continue;
                    }

                    if (creeps.harvesters < creepsPerClass.harvester) {
                        Creeps.createCreeps(spawnerName, "harvester", [WORK, CARRY, MOVE]);
                        continue;
                    }

                    if (creeps.upgraders < creepsPerClass.upgraders) {
                        Creeps.createCreeps(spawnerName, "upgrader", [WORK, CARRY, MOVE]);
                        continue;
                    }

                    if (creeps.builders < creepsPerClass.builders) {
                        Creeps.createCreeps(spawnerName, "builder", [WORK, CARRY, MOVE]);
                        continue;
                    }

                    if (creeps.repairers < creepsPerClass.repairers) {
                        Creeps.createCreeps(spawnerName, "repairer", [WORK, CARRY, MOVE]);
                        continue;
                    }
                }

                if (checkCounter === 250 || checkCounter === 500 || checkCounter === 750 || checkCounter === 950) {
                    Storage.SetValue("creepCount" + spawnerName, Counters.getCreepNumbers(spawnerName));
                } 
            }
        }
    }
}