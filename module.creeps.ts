import { Counters } from "./module.counters";
import { Storage } from "./module.storage";
import { Spawn } from "./module.spawn";

export module Creeps {
    export function createCreeps(spawn: string, classString: string, body: Array<BodyPartConstant>, gen: number = 1) {
        var random = Counters.getRandomInt(1, 100);

        while (Game.spawns[spawn].createCreep(body, classString + "lv:" + gen + "-" + random.toString(), {"class": classString}) == -3) {
            random = Counters.getRandomInt(1, 100);
        }

        Spawn.setSpawner(classString + "lv:" + gen + "-" + random.toString(), spawn);

        Storage.SetValue("creepCount" + spawn, Counters.getCreepNumbers(spawn));
    }
}