export module Counters {
    export function getCreepNumbers(spawn: string = null): ICreepCount {
        var builders = 0;
        var upgraders = 0;
        var harvesters = 0;
        var repairers = 0;
        var spawnFeeders = 0;

        for (let creepName in Game.creeps) {
            if (Game.creeps.hasOwnProperty(creepName)) {
                var creep = Game.creeps[creepName];

                if (creep.ticksToLive < 10) {
                    delete Game.creeps[creepName];
                    continue;
                }

                if (creep.memory.spawn !== spawn && spawn !== null) {
                    continue;
                }

                switch(creep.memory.class) {
                    case "harvester":
                    case "harvester2":
                        harvesters++;
                        break;
                    case "upgrader":
                    case "upgrader2":
                        upgraders++;
                        break;
                    case "builder":
                    case "builder2":
                        builders++;
                        break;
                    case "repairer":
                    case "repairer2":
                        repairers++;
                        break;
                    case "spawnFeeder":
                    case "spawnFeeder2":
                        spawnFeeders++;
                        break;
                    default:
                        break;
                }
            }
        }

        return {
            builders: builders,
            upgraders: upgraders,
            harvesters: harvesters,
            repairers: repairers,
            spawnFeeders: spawnFeeders
        }
    }

    export function getRandomInt(min, max): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
}