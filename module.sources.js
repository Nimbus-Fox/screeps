"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_counters_1 = require("./module.counters");
var module_storage_1 = require("./module.storage");
var Sources;
(function (Sources) {
    function assign(creep, newSource) {
        var assigned = module_storage_1.Storage.GetValue("assigned");
        var sources = creep.room.find(FIND_SOURCES);
        if (newSource || !assigned || !assigned[creep.id]) {
            assigned[creep.id] = sources[module_counters_1.Counters.getRandomInt(0, sources.length)].id;
            module_storage_1.Storage.SetValue("assigned", assigned);
        }
        creep.memory.source = Game.getObjectById(assigned[creep.id]);
    }
    Sources.assign = assign;
    if (!module_storage_1.Storage.Exists("assigned")) {
        module_storage_1.Storage.SetValue("assigned", {});
    }
})(Sources = exports.Sources || (exports.Sources = {}));
