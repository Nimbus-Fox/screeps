"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_storage_1 = require("./module.storage");
var Spawn;
(function (Spawn) {
    function getSpawner(creep) {
        return module_storage_1.Storage.GetValue("creepSpawners")[creep.name] || "Spawn1";
    }
    Spawn.getSpawner = getSpawner;
    function setSpawner(creep, spawn) {
        var spawners = module_storage_1.Storage.GetValue("creepSpawners");
        spawners[creep] = spawn;
        module_storage_1.Storage.SetValue("creepSpawners", spawners);
    }
    Spawn.setSpawner = setSpawner;
    if (!module_storage_1.Storage.Exists("creepSpawners")) {
        module_storage_1.Storage.SetValue("creepSpawners", {});
    }
})(Spawn = exports.Spawn || (exports.Spawn = {}));
