import { Containers } from "./module.containers";

export module Builder {
    export function assign(creep: Creep) {
        let localCreep = creep;
        creep.memory.runJob = () => {
            runJob(localCreep);
        }
    }

    function runJob(creep: Creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
        }

        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
        }

        Containers.assign(creep, !creep.memory.assigned);

        if (!creep.memory.restock) {
            var targets = creep.room.find(FIND_CONSTRUCTION_SITES) as Array<ConstructionSite>;

            var priority: ConstructionSite = targets[0];

            for (var name in targets) {
                if (targets.hasOwnProperty(name)) {
                    var site = targets[name] as ConstructionSite;
                    if (site.progress > priority.progress) {
                        priority = site;
                    }
                }
            }

            if (targets.length) {
                if (creep.build(priority) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(priority, { visualizePathStyle: { stroke: '#ffaa00' } });
                }
            }
        } else {
            if (creep.withdraw(creep.memory.container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
    }
}