import { Sources } from "./module.sources";
import { Containers } from "./module.containers";

export module Harvester {
    export function assign(creep: Creep) {
        let localCreep = creep;
        creep.memory.runJob = () => {
            runJob(localCreep);
        }
    }

    function runJob(creep: Creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
            creep.memory.assignedContainer = true;
        }

        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
            creep.memory.assignedContainer = false;
        }

        Sources.assign(creep, !creep.memory.assigned);
        Containers.assign(creep, !creep.memory.assignedContainer);

        if (creep.memory.restock) {

            if (creep.harvest(creep.memory.source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.source, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        } else {
            if (creep.transfer(creep.memory.container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
    }
}