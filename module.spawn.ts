import { Storage } from "./module.storage";

export module Spawn {
    export function getSpawner(creep: Creep): string {
        return Storage.GetValue<{[key: string]: string}>("creepSpawners")[creep.name] || "Spawn1";
    }

    export function setSpawner(creep: string, spawn: string) {
        var spawners = Storage.GetValue<{[key: string]: string}>("creepSpawners");

        spawners[creep] = spawn;

        Storage.SetValue("creepSpawners", spawners);
    }

    if (!Storage.Exists("creepSpawners")) {
        Storage.SetValue("creepSpawners", {});
    }
}