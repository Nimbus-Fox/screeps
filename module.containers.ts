import { Counters } from "./module.counters";
import { Storage } from "./module.storage";

export module Containers {
    export function assign(creep: Creep, newContainer: boolean) {
        var assigned = Storage.GetValue("containerAssignment");

        var structures = creep.room.find(FIND_STRUCTURES) as Array<Structure>;

        var containers: Array<Container> = [];

        for (var name in structures) {
            if (structures.hasOwnProperty(name)) {
                var structure = structures[name] as Structure;

                if (structure.structureType === STRUCTURE_CONTAINER) {
                    containers.push(structure as Container);
                }
            }
        }

        if (newContainer || !assigned || !assigned[creep.id]) {
            assigned[creep.id] = containers[Counters.getRandomInt(0, containers.length)].id;

            Storage.SetValue("containerAssignment", assigned);
        }
        
        creep.memory.container = Game.getObjectById(assigned[creep.id]) as Container;
    }

    if (!Storage.Exists("containerAssignement")) {
        Storage.SetValue("containerAssignment", {});
    }
}