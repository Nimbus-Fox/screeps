"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_containers_1 = require("./module.containers");
var DeathBed;
(function (DeathBed) {
    function assign(creep) {
        var localCreep = creep;
        creep.memory.runJob = function () {
            runJob(localCreep);
        };
    }
    DeathBed.assign = assign;
    function runJob(creep) {
        module_containers_1.Containers.assign(creep, false);
        if (creep.transfer(creep.memory.container, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            creep.moveTo(creep.memory.container);
        }
    }
})(DeathBed = exports.DeathBed || (exports.DeathBed = {}));
