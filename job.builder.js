"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var module_containers_1 = require("./module.containers");
var Builder;
(function (Builder) {
    function assign(creep) {
        var localCreep = creep;
        creep.memory.runJob = function () {
            runJob(localCreep);
        };
    }
    Builder.assign = assign;
    function runJob(creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
        }
        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
        }
        module_containers_1.Containers.assign(creep, !creep.memory.assigned);
        if (!creep.memory.restock) {
            var targets = creep.room.find(FIND_CONSTRUCTION_SITES);
            var priority = targets[0];
            for (var name in targets) {
                if (targets.hasOwnProperty(name)) {
                    var site = targets[name];
                    if (site.progress > priority.progress) {
                        priority = site;
                    }
                }
            }
            if (targets.length) {
                if (creep.build(priority) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(priority, { visualizePathStyle: { stroke: '#ffaa00' } });
                }
            }
        }
        else {
            if (creep.withdraw(creep.memory.container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
    }
})(Builder = exports.Builder || (exports.Builder = {}));
