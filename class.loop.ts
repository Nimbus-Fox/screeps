import { Harvester } from "./job.harvester";
import { Upgrader } from "./job.upgrader";
import { Builder } from "./job.builder";
import { Repairer } from "./job.repairer";
import { SpawnFeeder } from "./job.spawnFeeder";
import { DeathBed } from "./job.deathBed";
import { Spawner } from "./module.spawner";
import { Spawn } from "./module.spawn";
import { Creeps } from "./module.creeps";
import { Storage } from "./module.storage";

/*setTimeout(() => {
        for (let name in Game.creeps) {
            if (Game.creeps.hasOwnProperty(name)) {
                let creep = Game.creeps[name];

                Creeps.assignSource(creep, creep.room.find(FIND_SOURCES));
            }
        }
}, 600000);*/

export module Loop {
    export function loop() {

        for (let name in Game.creeps) {
            if (Game.creeps.hasOwnProperty(name)) {
                let creep = Game.creeps[name];

                if (creep.ticksToLive < 50) {
                    creep.memory.class = "deathBed";
                }

                creep.memory.spawn = Spawn.getSpawner(creep);

                switch (creep.memory.class) {
                    case "harvester":
                        Harvester.assign(creep);
                        break;
                    case "upgrader":
                        Upgrader.assign(creep);
                        break;
                    case "builder":
                        Builder.assign(creep);
                        break;
                    case "repairer":
                        Repairer.assign(creep);
                        break;
                    case "spawnFeeder":
                        SpawnFeeder.assign(creep);
                        break;
                    case "deathBed":
                        DeathBed.assign(creep);
                        break;
                    default:
                        break;
                }

                if (creep.memory.runJob) {
                    creep.memory.runJob()
                }
            }
        }

        var checkCounter = Storage.GetValue<number>("checkCounter");

        Storage.SetValue("checkCounter", checkCounter + 1);

        if (checkCounter === 1000) {
            Storage.SetValue("checkCounter", 0);
            StorageCheck();
        }

        Spawner.spawn();
    }

    if (!Storage.Exists("checkCounter")) {
        Storage.SetValue("checkCounter", 0);
    }

    function StorageCheck() {
        sourceAssignCheck();
        containerAssignCheck();
        spawnerAssignCheck();
    }

    function sourceAssignCheck() {
        var assigned = Storage.GetValue<{ [key: string]: number }>("assigned");

        for (var key in assigned) {
            if (assigned.hasOwnProperty(key)) {
                let deleteCreep = true;
                for (var name in Game.creeps) {
                    if (Game.creeps.hasOwnProperty(name)) {
                        if (Game.creeps[name].id === key) {
                            deleteCreep = false;
                        }
                    }
                }

                if (deleteCreep) {
                    delete assigned[key];
                }
            }
        }

        Storage.SetValue("assigned", assigned);
    }

    function containerAssignCheck() {
        var assigned = Storage.GetValue<{ [key: string]: number }>("containerAssignment");

        for (var key in assigned) {
            if (assigned.hasOwnProperty(key)) {
                let deleteCreep = true;
                for (var name in Game.creeps) {
                    if (Game.creeps.hasOwnProperty(name)) {
                        if (Game.creeps[name].id === key) {
                            deleteCreep = false;
                        }
                    }
                }

                if (deleteCreep) {
                    delete assigned[key];
                }
            }
        }

        Storage.SetValue("containerAssignment", assigned);
    }

    function spawnerAssignCheck() {
        var assigned = Storage.GetValue<{ [key: string]: string }>("creepSpawners");

        for (var key in assigned) {
            if (assigned.hasOwnProperty(key)) {
                let deleteCreep = true;
                for (var name in Game.creeps) {
                    if (Game.creeps.hasOwnProperty(name)) {
                        if (Game.creeps[name].name === key) {
                            deleteCreep = false;
                        }
                    }
                }

                if (deleteCreep) {
                    delete assigned[key];
                }
            }
        }
    }
} 