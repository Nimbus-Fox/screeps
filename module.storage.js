"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Storage;
(function (Storage) {
    function GetValue(key) {
        return Memory.storage[key];
    }
    Storage.GetValue = GetValue;
    function SetValue(key, value) {
        Memory.storage[key] = value;
    }
    Storage.SetValue = SetValue;
    function Exists(key) {
        return Memory.storage[key] !== undefined;
    }
    Storage.Exists = Exists;
    if (Memory.storage === undefined) {
        Memory.storage = {};
    }
})(Storage = exports.Storage || (exports.Storage = {}));
