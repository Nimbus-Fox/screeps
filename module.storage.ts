export module Storage {
    export function GetValue<T>(key: string): T {
        return Memory.storage[key];
    }

    export function SetValue(key: string, value: any) {
        Memory.storage[key] = value;
    }

    export function Exists(key: string): boolean {
        return Memory.storage[key] !== undefined;
    }

    if (Memory.storage === undefined) {
        Memory.storage = {} as any;
    }
}