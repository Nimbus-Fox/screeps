import { Containers } from "./module.containers";

export module Upgrader {
    export function assign(creep: Creep) {
        let localCreep = creep;
        creep.memory.runJob = () => {
            runJob(localCreep);
        }
    }

    function runJob(creep: Creep) {
        if (creep.memory.restock && creep.carry.energy === creep.carryCapacity) {
            creep.memory.restock = false;
            creep.memory.assigned = false;
        }

        if (!creep.memory.restock && creep.carry.energy === 0) {
            creep.memory.restock = true;
            creep.memory.assigned = true;
        }

        Containers.assign(creep, !creep.memory.assigned);

        if (creep.memory.restock) {
            if (creep.withdraw(creep.memory.container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.memory.container, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        } else {
            if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller, { visualizePathStyle: { stroke: '#ffaa00' } });
            }
        }
    }
}